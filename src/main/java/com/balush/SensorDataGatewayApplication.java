package com.balush;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SensorDataGatewayApplication {
	public static void main(String[] args) {
		SpringApplication.run(SensorDataGatewayApplication.class, args);
	}
}
