package com.balush.validator;

import com.balush.config.DataType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class DataTypeValidator implements ConstraintValidator<DataTypeValidation, String> {
    @Override
    public boolean isValid(String type, ConstraintValidatorContext constraintValidatorContext) {
        if (type == null || type.isEmpty()) {
            return false;
        }

        return isAllowedType(type);
    }

    private boolean isAllowedType(String type) {
        return Arrays.stream(DataType.values()).anyMatch(dataType -> dataType.getDataName().equalsIgnoreCase(type));
    }
}
