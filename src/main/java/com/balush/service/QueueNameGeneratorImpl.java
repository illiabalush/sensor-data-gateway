package com.balush.service;

import org.springframework.stereotype.Component;

@Component
public class QueueNameGeneratorImpl implements QueueNameGenerator {
    @Override
    public String generateQueueName(String prefix) {
        return prefix + "-data-queue";
    }
}
