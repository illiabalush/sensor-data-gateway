package com.balush.service;

public interface QueueNameGenerator {

    String generateQueueName(String prefix);

}
