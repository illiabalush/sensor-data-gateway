package com.balush.controller;

import com.balush.model.Data;
import com.balush.service.QueueNameGenerator;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.balush.config.Urls.DATA_GATEWAY;

@Log4j2
@RestController
@AllArgsConstructor
public class DataController {

    private final JmsTemplate template;
    private final QueueNameGenerator queueNameGenerator;

    @PostMapping(DATA_GATEWAY)
    public void receiveIncomingData(@Valid @RequestBody Data data) {
        log.info("Received data: " + data);
        template.convertAndSend(queueNameGenerator.generateQueueName(data.getType()), data);
    }

}
