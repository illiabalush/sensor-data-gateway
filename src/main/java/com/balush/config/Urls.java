package com.balush.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class Urls {

    private final static String API = "/api";
    public final static String DATA_GATEWAY = API + "/gateway";

}
