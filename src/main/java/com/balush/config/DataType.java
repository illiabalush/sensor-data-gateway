package com.balush.config;

public enum DataType {
    TEMPERATURE("temperature"), WINDITY("windity"), HUMIDITY("humidity");

    private String dataName;

    DataType(String dataName) {
        this.dataName = dataName;
    }

    public String getDataName() {
        return dataName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

}
