package com.balush.model;

import com.balush.validator.DataTypeValidation;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Data {

    @DataTypeValidation(message = "Not valid type format")
    @JsonProperty("type")
    private String type;

    @NotNull(message = "Value cannot be null")
    @JsonProperty("value")
    private Float value;

    @NotNull(message = "Coordinates cannot be null")
    @JsonProperty("coordinates")
    private Coordinates coordinates;

    @JsonProperty("timestamp")
    private long timestamp = System.currentTimeMillis();

}
